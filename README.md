# ES6 + WebPack + Karma
Write ES6 modules, lint with JSHint, bundle with webpack then test with Karma-Jasmine.
