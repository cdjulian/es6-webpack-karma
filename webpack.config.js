var glob = require("glob");

module.exports = {
    devtool: 'source-map',

    entry: {
        KanjiCanvas: './script/index.js',
        Spec: glob.sync('./script/test/*.js')
    },
    output: {
        path: __dirname + '/script/bundle',
        filename: '[name].bundle.js'
    },
    module: {
        preLoaders: [
            {
                test: /\.js$/,
                loader: "jshint-loader"
            }
        ],
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader'
            }
        ]
    },

    jshint: {
        esnext: true
    }
};2