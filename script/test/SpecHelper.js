// set fixtures
jasmine.getFixtures().fixturesPath = 'base/script/test/fixtures';

// programatically dispatch mouse events
export default function dispatchMouseEvent(el, type, x, y) {
    if(typeof MouseEvent == 'function') {
        var e = new MouseEvent(type, {
            bubbles: true,
            cancelable: true,
            view: window,
            screenX: x,
            screenY: y,
            clientX: x,
            clientY: y
        });
        el.dispatchEvent(e);

    } else {
        var e2 = document.createEvent("MouseEvents");
        e2.initMouseEvent(type, true, true, window, 0, x, y, x, y, false, false, false, false, 0, null);
        el.dispatchEvent(e2);
    }
}

//beforeEach(function () {
//  jasmine.addMatchers({
//    toBePlaying: function () {
//      return {
//        compare: function (actual, expected) {
//          var player = actual;
//
//          return {
//            pass: player.currentlyPlayingSong === expected && player.isPlaying
//          }
//        }
//      };
//    }
//  });
//});