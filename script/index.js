import KanjiCanvas from './KanjiCanvas.js';
var canvas = document.getElementById('KanjiCanvas');
if(canvas) {
    var kc = new KanjiCanvas(canvas);
    kc.onSubmit(function (data) {
        console.log(data.strokes);
    });
}