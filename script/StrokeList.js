export default class StrokeList {

    constructor() {
        this.strokes = [];
    }

    add(stroke) {
        this.strokes.push(stroke);
    }

    area() {
        return (this.maxX() - this.minX()) * (this.maxY() - this.minY());
    }

    maxX() {
        var i, temp, x = null;
        for(i = 0; i < this.strokes.length; i++){
            temp = this.strokes[i].maxX();
            if(x === null || temp > x) {
                x = temp;
            }
        }
        return x;
    }

    maxY() {
        var i, temp, y = null;
        for(i = 0; i < this.strokes.length; i++){
            temp = this.strokes[i].maxY();
            if(y === null || temp > y) {
                y = temp;
            }
        }
        return y;
    }

    minX() {
        var i, temp, x = null;
        for(i = 0; i < this.strokes.length; i++){
            temp = this.strokes[i].minX();
            if(x === null || temp < x) {
                x = temp;
            }
        }
        return x;
    }

    minY() {
        var i, temp, y = null;
        for(i = 0; i < this.strokes.length; i++){
            temp = this.strokes[i].minY();
            if(y === null || temp < y) {
                y = temp;
            }
        }
        return y;
    }

    clear() {
        this.strokes = [];
    }
}