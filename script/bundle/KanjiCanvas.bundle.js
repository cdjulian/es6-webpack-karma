/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _KanjiCanvasJs = __webpack_require__(7);
	
	var _KanjiCanvasJs2 = _interopRequireDefault(_KanjiCanvasJs);
	
	var canvas = document.getElementById('KanjiCanvas');
	if (canvas) {
	    var kc = new _KanjiCanvasJs2['default'](canvas);
	    kc.onSubmit(function (data) {
	        console.log(data.strokes);
	    });
	}

/***/ },
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	exports.__esModule = true;
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _pointJs = __webpack_require__(8);
	
	var _pointJs2 = _interopRequireDefault(_pointJs);
	
	var _StrokeJs = __webpack_require__(9);
	
	var _StrokeJs2 = _interopRequireDefault(_StrokeJs);
	
	var _StrokeListJs = __webpack_require__(10);
	
	var _StrokeListJs2 = _interopRequireDefault(_StrokeListJs);
	
	var _DrawingCanvasJs = __webpack_require__(11);
	
	var _DrawingCanvasJs2 = _interopRequireDefault(_DrawingCanvasJs);
	
	var KanjiCanvas = (function () {
		function KanjiCanvas(el) {
			_classCallCheck(this, KanjiCanvas);
	
			this.kc = el;
			this.canvas = new _DrawingCanvasJs2['default'](el.querySelector('canvas'));
			this.clearBtn = this.kc.querySelector('.clear');
			this.submitBtn = this.kc.querySelector('.submit');
			this.strokeList = new _StrokeListJs2['default']();
			this.currStroke = null;
			this.canvas.onDrawStart(this.drawStartHandler.bind(this));
			this.canvas.onDraw(this.drawHandler.bind(this));
			this.canvas.onDrawEnd(this.drawEndHandler.bind(this));
			this.submitCallback = null;
			this.clearBtn.addEventListener('click', this.clear.bind(this));
			this.submitBtn.addEventListener('click', this.submit.bind(this));
		}
	
		KanjiCanvas.prototype.clear = function clear() {
			this.canvas.clear();
			this.strokeList.clear();
		};
	
		KanjiCanvas.prototype.submit = function submit() {
			if (typeof this.submitCallback === 'function') {
				this.submitCallback(this.strokeList);
			}
		};
	
		KanjiCanvas.prototype.drawStartHandler = function drawStartHandler(x, y) {
			this.currStroke = new _StrokeJs2['default'](_pointJs2['default'](x, y));
		};
	
		KanjiCanvas.prototype.drawHandler = function drawHandler(x, y) {
			if (this.currStroke) {
				this.currStroke.add(_pointJs2['default'](x, y));
			}
		};
	
		KanjiCanvas.prototype.drawEndHandler = function drawEndHandler() {
			if (this.currStroke) {
				this.strokeList.add(this.currStroke);
			}
		};
	
		KanjiCanvas.prototype.onSubmit = function onSubmit(callback) {
			this.submitCallback = callback;
		};
	
		return KanjiCanvas;
	})();
	
	exports['default'] = KanjiCanvas;
	module.exports = exports['default'];

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	exports.__esModule = true;
	exports['default'] = p;
	
	function p(x, y) {
		if (arguments.length !== 2) {
			throw new Error('point requires 2 coordinates');
		}
		if (isNaN(x) || isNaN(y)) {
			throw new TypeError('point coordinate is not a number');
		}
		return { 'x': x, 'y': y };
	}
	
	module.exports = exports['default'];

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Stroke = (function () {
		function Stroke() {
			_classCallCheck(this, Stroke);
	
			this.points = arguments.length === 0 ? [] : Array.prototype.slice.call(arguments);
			this.angle = NaN;
		}
	
		Stroke.prototype.add = function add(point) {
			this.points.push(point);
			if (this.points.length > 1) {
				this.angle = this.getAngle();
			}
		};
	
		Stroke.prototype.maxX = function maxX() {
			var i,
			    x = null;
			for (i = 0; i < this.points.length; i++) {
				if (x === null || this.points[i].x > x) {
					x = this.points[i].x;
				}
			}
			return x;
		};
	
		Stroke.prototype.minX = function minX() {
			var i,
			    x = null;
			for (i = 0; i < this.points.length; i++) {
				if (x === null || this.points[i].x < x) {
					x = this.points[i].x;
				}
			}
			return x;
		};
	
		Stroke.prototype.maxY = function maxY() {
			var i,
			    y = null;
			for (i = 0; i < this.points.length; i++) {
				if (y === null || this.points[i].y > y) {
					y = this.points[i].y;
				}
			}
			return y;
		};
	
		Stroke.prototype.minY = function minY() {
			var i,
			    y = null;
			for (i = 0; i < this.points.length; i++) {
				if (y === null || this.points[i].y < y) {
					y = this.points[i].y;
				}
			}
			return y;
		};
	
		Stroke.prototype.area = function area() {
			return (this.maxX() - this.minX()) * (this.maxY() - this.minY());
		};
	
		Stroke.prototype.getAngle = function getAngle() {
			var p1 = this.points[0],
			    p2 = this.points[this.points.length - 1];
			return Math.atan2(p2.y - p1.y, p2.x - p1.x) * 180 / Math.PI;
		};
	
		return Stroke;
	})();
	
	exports["default"] = Stroke;
	module.exports = exports["default"];

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var StrokeList = (function () {
	    function StrokeList() {
	        _classCallCheck(this, StrokeList);
	
	        this.strokes = [];
	    }
	
	    StrokeList.prototype.add = function add(stroke) {
	        this.strokes.push(stroke);
	    };
	
	    StrokeList.prototype.area = function area() {
	        return (this.maxX() - this.minX()) * (this.maxY() - this.minY());
	    };
	
	    StrokeList.prototype.maxX = function maxX() {
	        var i,
	            temp,
	            x = null;
	        for (i = 0; i < this.strokes.length; i++) {
	            temp = this.strokes[i].maxX();
	            if (x === null || temp > x) {
	                x = temp;
	            }
	        }
	        return x;
	    };
	
	    StrokeList.prototype.maxY = function maxY() {
	        var i,
	            temp,
	            y = null;
	        for (i = 0; i < this.strokes.length; i++) {
	            temp = this.strokes[i].maxY();
	            if (y === null || temp > y) {
	                y = temp;
	            }
	        }
	        return y;
	    };
	
	    StrokeList.prototype.minX = function minX() {
	        var i,
	            temp,
	            x = null;
	        for (i = 0; i < this.strokes.length; i++) {
	            temp = this.strokes[i].minX();
	            if (x === null || temp < x) {
	                x = temp;
	            }
	        }
	        return x;
	    };
	
	    StrokeList.prototype.minY = function minY() {
	        var i,
	            temp,
	            y = null;
	        for (i = 0; i < this.strokes.length; i++) {
	            temp = this.strokes[i].minY();
	            if (y === null || temp < y) {
	                y = temp;
	            }
	        }
	        return y;
	    };
	
	    StrokeList.prototype.clear = function clear() {
	        this.strokes = [];
	    };
	
	    return StrokeList;
	})();
	
	exports["default"] = StrokeList;
	module.exports = exports["default"];

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	exports.__esModule = true;
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var DrawingCanvas = (function () {
	    function DrawingCanvas(el) {
	        _classCallCheck(this, DrawingCanvas);
	
	        this.c = el;
	        this.ctx = this.c.getContext('2d');
	        this.moveHandler = null;
	        this.upHandler = null;
	        this.outHandler = null;
	        this.drawStartCallback = null;
	        this.drawCallback = null;
	        this.drawEndCallback = null;
	        this.c.addEventListener('mousedown', this.drawStart.bind(this), false);
	
	        // canvas and line style
	        this.c.height = this.c.clientHeight;
	        this.c.width = this.c.clientWidth;
	        this.ctx.lineCap = 'round';
	        this.ctx.lineJoin = 'round';
	        this.ctx.lineWidth = 3;
	    }
	
	    // given a mouse event, returns mouse coordinates relative to the canvas
	
	    DrawingCanvas.prototype.getOffset = function getOffset(e) {
	        var rect = e.target.getBoundingClientRect();
	        return {
	            x: e.offsetX || e.pageX - rect.left - window.scrollX,
	            y: e.offsetY || e.pageY - rect.top - window.scrollY
	        };
	    };
	
	    // initializes canvas path, registers handlers for other mouse events
	
	    DrawingCanvas.prototype.drawStart = function drawStart(e) {
	        var p = this.getOffset(e);
	        this.c.addEventListener('mousemove', this.moveHandler = this.draw.bind(this), false);
	        this.c.addEventListener('mouseup', this.upHandler = this.drawEnd.bind(this), false);
	        this.c.addEventListener('mouseout', this.outHandler = this.drawEnd.bind(this), false);
	        this.ctx.beginPath();
	        this.ctx.moveTo(p.x, p.y);
	        this.ctx.lineTo(p.x, p.y);
	        this.ctx.stroke();
	        if (typeof this.drawStartCallback === 'function') {
	            this.drawStartCallback(p.x, p.y);
	        }
	    };
	
	    // continues the path to the current mouse coordinates and draws it
	
	    DrawingCanvas.prototype.draw = function draw(e) {
	        var p = this.getOffset(e);
	        this.ctx.lineTo(p.x, p.y);
	        this.ctx.stroke();
	        if (typeof this.drawCallback === 'function') {
	            this.drawCallback(p.x, p.y);
	        }
	    };
	
	    // removes the mouse event handlers
	
	    DrawingCanvas.prototype.drawEnd = function drawEnd(e) {
	        this.c.removeEventListener('mousemove', this.moveHandler);
	        this.c.removeEventListener('mouseup', this.upHandler);
	        this.c.removeEventListener('mouseout', this.outHandler);
	        this.upHandler = null;
	        this.moveHandler = null;
	        this.outHandler = null;
	        if (typeof this.drawEndCallback === 'function') {
	            this.drawEndCallback();
	        }
	    };
	
	    DrawingCanvas.prototype.clear = function clear() {
	        this.ctx.clearRect(0, 0, this.c.width, this.c.height);
	    };
	
	    DrawingCanvas.prototype.onDrawStart = function onDrawStart(callback) {
	        this.drawStartCallback = callback;
	    };
	
	    DrawingCanvas.prototype.onDraw = function onDraw(callback) {
	        this.drawCallback = callback;
	    };
	
	    DrawingCanvas.prototype.onDrawEnd = function onDrawEnd(callback) {
	        this.drawEndCallback = callback;
	    };
	
	    return DrawingCanvas;
	})();
	
	exports['default'] = DrawingCanvas;
	module.exports = exports['default'];

/***/ }
/******/ ]);
//# sourceMappingURL=KanjiCanvas.bundle.js.map