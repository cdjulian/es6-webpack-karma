/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(2);
	__webpack_require__(3);
	__webpack_require__(4);
	__webpack_require__(5);
	__webpack_require__(1);
	module.exports = __webpack_require__(6);


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _pointJs = __webpack_require__(8);
	
	var _pointJs2 = _interopRequireDefault(_pointJs);
	
	var _StrokeJs = __webpack_require__(9);
	
	var _StrokeJs2 = _interopRequireDefault(_StrokeJs);
	
	describe('Stroke', function () {
	    var stroke;
	
	    beforeEach(function () {
	        stroke = new _StrokeJs2['default']();
	    });
	
	    it('should exist', function () {
	        expect(stroke).toExist();
	    });
	
	    it('should allow you to add points on instantiation', function () {
	        var p1 = _pointJs2['default'](1, 1),
	            p2 = _pointJs2['default'](2, 2),
	            s = new _StrokeJs2['default'](p1, p2);
	
	        expect(s.points).toContain(p1);
	        expect(s.points).toContain(p2);
	    });
	
	    it('should allow you to add points after instantiation', function () {
	        var p1 = _pointJs2['default'](1, 1);
	        stroke.add(p1);
	        expect(stroke.points).toContain(p1);
	    });
	
	    it('should maintain the order of points added', function () {
	        var p1 = _pointJs2['default'](1, 1),
	            p2 = _pointJs2['default'](2, 2),
	            p3 = _pointJs2['default'](3, 3);
	        stroke.add(p1);
	        stroke.add(p2);
	        stroke.add(p3);
	        expect(stroke.points[0]).toEqual(p1);
	        expect(stroke.points[1]).toEqual(p2);
	        expect(stroke.points[2]).toEqual(p3);
	    });
	
	    it('should be able to return the angle ' + 'of the line drawn between the start and end points', function () {
	        var p1 = _pointJs2['default'](0, 1),
	            p2 = _pointJs2['default'](0, 2),
	            p3 = _pointJs2['default'](0, 3);
	        stroke.add(p1);
	        stroke.add(p2);
	        stroke.add(p3);
	        expect(stroke.angle).toBeCloseTo(90, 0);
	    });
	
	    it('should be able to return the area of the bounding box of the stroke', function () {
	        var p1 = _pointJs2['default'](0, 6),
	            p2 = _pointJs2['default'](3, 3),
	            p3 = _pointJs2['default'](-4, -1);
	        stroke.add(p1);
	        stroke.add(p2);
	        stroke.add(p3);
	        expect(stroke.area()).toEqual(49);
	    });
	
	    describe('max min functions', function () {
	        beforeEach(function () {
	            var p1 = _pointJs2['default'](0, 1),
	                p2 = _pointJs2['default'](1, 2),
	                p3 = _pointJs2['default'](2, 3);
	            stroke.add(p1);
	            stroke.add(p2);
	            stroke.add(p3);
	        });
	        it('should be able to return its max x coordinate', function () {
	            expect(stroke.maxX()).toEqual(2);
	        });
	        it('should be able to return its min x coordinate', function () {
	            expect(stroke.minX()).toEqual(0);
	        });
	        it('should be able to return its max y coordinate', function () {
	            expect(stroke.maxY()).toEqual(3);
	        });
	        it('should be able to return its min y coordinate', function () {
	            expect(stroke.minY()).toEqual(1);
	        });
	    });
	});

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _SpecHelperJs = __webpack_require__(4);
	
	var _SpecHelperJs2 = _interopRequireDefault(_SpecHelperJs);
	
	var _pointJs = __webpack_require__(8);
	
	var _pointJs2 = _interopRequireDefault(_pointJs);
	
	var _StrokeJs = __webpack_require__(9);
	
	var _StrokeJs2 = _interopRequireDefault(_StrokeJs);
	
	var _StrokeListJs = __webpack_require__(10);
	
	var _StrokeListJs2 = _interopRequireDefault(_StrokeListJs);
	
	var _DrawingCanvasJs = __webpack_require__(11);
	
	var _DrawingCanvasJs2 = _interopRequireDefault(_DrawingCanvasJs);
	
	describe('Drawing Canvas', function () {
	    var can;
	    beforeEach(function () {
	        loadFixtures('DrawingCanvasFixture.html');
	        can = new _DrawingCanvasJs2['default'](document.querySelector('.canvas'));
	    });
	    it('should store a reference to its canvas element', function () {
	        expect(can.c).toExist();
	    });
	    it('should store a reference to its canvas context', function () {
	        expect(can.ctx).toExist();
	    });
	    it('should allow you to register a drawStart callback', function () {
	        var cb = function cb() {
	            return;
	        };
	        can.onDrawStart(cb);
	        expect(can.drawStartCallback).toEqual(cb);
	    });
	    it('should allow you to register a draw callback', function () {
	        var cb = function cb() {
	            return;
	        };
	        can.onDraw(cb);
	        expect(can.drawCallback).toEqual(cb);
	    });
	    it('should allow you to register a drawEnd callback', function () {
	        var cb = function cb() {
	            return;
	        };
	        can.onDrawEnd(cb);
	        expect(can.drawEndCallback).toEqual(cb);
	    });
	    it('should call the drawStartCallback, passing the mousedown coordinates', function (done) {
	        var xy,
	            cb = function cb(x, y) {
	            xy = { 'x': x, 'y': y };
	        };
	        can.onDrawStart(cb);
	        can.drawStart({ offsetX: 10, offsetY: 22, target: { getBoundingClientRect: function getBoundingClientRect() {} } });
	        expect(xy).toEqual({ 'x': 10, 'y': 22 });
	        done();
	    });
	    it('should call the drawCallback, passing the mousemove coordinates', function (done) {
	        var xy,
	            cb = function cb(x, y) {
	            xy = { 'x': x, 'y': y };
	        };
	        can.onDraw(cb);
	        can.draw({ offsetX: 11, offsetY: 23, target: { getBoundingClientRect: function getBoundingClientRect() {} } });
	        expect(xy).toEqual({ 'x': 11, 'y': 23 });
	        done();
	    });
	    it('should call the drawEndCallback', function () {
	        var cb = { foo: function foo() {
	                return 0;
	            } },
	            mouse = document.createEvent('MouseEvents');
	        spyOn(cb, 'foo');
	        can.onDrawEnd(cb.foo);
	        //mouse.initEvent('mousedown', true, true);
	        //can.c.dispatchEvent(mouse);
	        //mouse.initEvent('mouseup', true, true);
	        //can.c.dispatchEvent(mouse);
	        var rect = can.c.getBoundingClientRect();
	        var canvasLeft = rect.left - window.scrollX;
	        var canvasTop = rect.top - window.scrollY;
	        _SpecHelperJs2['default'](can.c, 'mousedown', canvasLeft, canvasTop);
	        _SpecHelperJs2['default'](can.c, 'mouseup', canvasLeft + 10, canvasTop + 10);
	        expect(cb.foo).toHaveBeenCalled();
	    });
	});

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _SpecHelperJs = __webpack_require__(4);
	
	var _SpecHelperJs2 = _interopRequireDefault(_SpecHelperJs);
	
	var _pointJs = __webpack_require__(8);
	
	var _pointJs2 = _interopRequireDefault(_pointJs);
	
	var _StrokeJs = __webpack_require__(9);
	
	var _StrokeJs2 = _interopRequireDefault(_StrokeJs);
	
	var _StrokeListJs = __webpack_require__(10);
	
	var _StrokeListJs2 = _interopRequireDefault(_StrokeListJs);
	
	var _DrawingCanvasJs = __webpack_require__(11);
	
	var _DrawingCanvasJs2 = _interopRequireDefault(_DrawingCanvasJs);
	
	var _KanjiCanvasJs = __webpack_require__(7);
	
	var _KanjiCanvasJs2 = _interopRequireDefault(_KanjiCanvasJs);
	
	describe('KanjiCanvas', function () {
	    var kc, canvasEl, canvasTop, canvasLeft;
	
	    beforeEach(function () {
	        loadFixtures('KanjiCanvasFixture.html');
	        kc = new _KanjiCanvasJs2['default'](document.getElementById('KanjiCanvas'));
	        canvasEl = kc.kc.querySelector('canvas');
	        var rect = canvasEl.getBoundingClientRect();
	        canvasLeft = rect.left - window.scrollX;
	        canvasTop = rect.top - window.scrollY;
	    });
	
	    it('should have a DrawingCanvas', function () {
	        expect(kc.canvas).toEqual(jasmine.any(_DrawingCanvasJs2['default']));
	    });
	    it('should have a Clear button', function () {
	        expect(kc.clearBtn).toEqual(kc.kc.querySelector('.clear'));
	    });
	    it('should have a Submit button', function () {
	        expect(kc.submitBtn).toEqual(kc.kc.querySelector('.submit'));
	    });
	    it('should have a StrokeList', function () {
	        expect(kc.strokeList).toEqual(jasmine.any(_StrokeListJs2['default']));
	    });
	
	    describe('event handling', function () {
	        beforeEach(function () {
	            _SpecHelperJs2['default'](canvasEl, 'mousedown', canvasLeft, canvasTop);
	            _SpecHelperJs2['default'](canvasEl, 'mousemove', canvasLeft + 10, canvasTop + 10);
	            _SpecHelperJs2['default'](canvasEl, 'mouseup', canvasLeft + 10, canvasTop + 10);
	        });
	
	        it('should add a stroke to StrokeList when a stroke is drawn', function () {
	            expect(kc.strokeList.strokes.length).toEqual(1);
	        });
	        it('should clear its strokeList when Clear is clicked', function () {
	            expect(kc.strokeList.strokes.length).toEqual(1);
	            _SpecHelperJs2['default'](kc.clearBtn, 'click', 0, 0);
	            expect(kc.strokeList.strokes.length).toEqual(0);
	        });
	        it('should fire a callback on submit, passing the current strokeList', function () {
	            var r;
	            var foo = {
	                bar: function bar(data) {
	                    r = data;
	                }
	            };
	            spyOn(foo, 'bar').and.callThrough();
	            kc.onSubmit(foo.bar);
	            kc.submitBtn.addEventListener('click', function () {
	                return;
	            });
	            _SpecHelperJs2['default'](kc.submitBtn, 'click', 0, 0);
	            expect(foo.bar).toHaveBeenCalled();
	            expect(r).toEqual(jasmine.any(_StrokeListJs2['default']));
	        });
	    });
	});

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	exports.__esModule = true;
	
	// programatically dispatch mouse events
	exports['default'] = dispatchMouseEvent;
	// set fixtures
	jasmine.getFixtures().fixturesPath = 'base/script/test/fixtures';
	function dispatchMouseEvent(el, type, x, y) {
	    if (typeof MouseEvent == 'function') {
	        var e = new MouseEvent(type, {
	            bubbles: true,
	            cancelable: true,
	            view: window,
	            screenX: x,
	            screenY: y,
	            clientX: x,
	            clientY: y
	        });
	        el.dispatchEvent(e);
	    } else {
	        var e2 = document.createEvent('MouseEvents');
	        e2.initMouseEvent(type, true, true, window, 0, x, y, x, y, false, false, false, false, 0, null);
	        el.dispatchEvent(e2);
	    }
	}
	
	//beforeEach(function () {
	//  jasmine.addMatchers({
	//    toBePlaying: function () {
	//      return {
	//        compare: function (actual, expected) {
	//          var player = actual;
	//
	//          return {
	//            pass: player.currentlyPlayingSong === expected && player.isPlaying
	//          }
	//        }
	//      };
	//    }
	//  });
	//});
	
	module.exports = exports['default'];

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _pointJs = __webpack_require__(8);
	
	var _pointJs2 = _interopRequireDefault(_pointJs);
	
	var _StrokeJs = __webpack_require__(9);
	
	var _StrokeJs2 = _interopRequireDefault(_StrokeJs);
	
	var _StrokeListJs = __webpack_require__(10);
	
	var _StrokeListJs2 = _interopRequireDefault(_StrokeListJs);
	
	describe('StrokeList', function () {
		var strokeList;
	
		beforeEach(function () {
			strokeList = new _StrokeListJs2['default']();
		});
	
		it('should have an empty stroke list when instantiated', function () {
			expect(strokeList.strokes).toEqual([]);
		});
	
		it('should allow a stroke to be added', function () {
			var stroke1 = new _StrokeJs2['default'](_pointJs2['default'](0, 0), _pointJs2['default'](1, 1));
			strokeList.add(stroke1);
			expect(strokeList.strokes).toContain(stroke1);
		});
	
		it('should maintain the order of strokes added', function () {
			var stroke1 = new _StrokeJs2['default'](_pointJs2['default'](0, 0), _pointJs2['default'](1, 1)),
			    stroke2 = new _StrokeJs2['default'](_pointJs2['default'](1, 1), _pointJs2['default'](2, 2)),
			    stroke3 = new _StrokeJs2['default'](_pointJs2['default'](-3, -3), _pointJs2['default'](0, 10));
			strokeList.add(stroke1);
			strokeList.add(stroke2);
			strokeList.add(stroke3);
			expect(strokeList.strokes[0]).toEqual(stroke1);
			expect(strokeList.strokes[1]).toEqual(stroke2);
			expect(strokeList.strokes[2]).toEqual(stroke3);
		});
	
		it('should be able to return the area of the bounding box around all strokes', function () {
			var stroke1 = new _StrokeJs2['default'](_pointJs2['default'](0, 0), _pointJs2['default'](1, 1)),
			    stroke2 = new _StrokeJs2['default'](_pointJs2['default'](1, 1), _pointJs2['default'](2, 2)),
			    stroke3 = new _StrokeJs2['default'](_pointJs2['default'](-5, -5), _pointJs2['default'](-5, 5));
			strokeList.add(stroke1);
			expect(strokeList.area()).toEqual(1);
			strokeList.add(stroke2);
			expect(strokeList.area()).toEqual(4);
			strokeList.add(stroke3);
			expect(strokeList.area()).toEqual(70);
		});
	});

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _pointJs = __webpack_require__(8);
	
	var _pointJs2 = _interopRequireDefault(_pointJs);
	
	describe('point', function () {
		it('should take x & y coordinates and return an object containing both coordinates', function () {
			var p1 = _pointJs2['default'](0, 0);
			expect(p1).toEqual({ x: 0, y: 0 });
		});
	
		it('should throw an error if either coordinate is not a number', function () {
			expect(function () {
				_pointJs2['default']('a', 1);
			}).toThrowError(TypeError);
			expect(function () {
				_pointJs2['default'](1, 'a');
			}).toThrowError(TypeError);
		});
	
		it('should throw an error if it is not passed exactly 2 coordinates', function () {
			expect(function () {
				_pointJs2['default'](1);
			}).toThrowError(Error);
			expect(function () {
				_pointJs2['default'](1, 2, 3);
			}).toThrowError(Error);
		});
	});

/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	exports.__esModule = true;
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var _pointJs = __webpack_require__(8);
	
	var _pointJs2 = _interopRequireDefault(_pointJs);
	
	var _StrokeJs = __webpack_require__(9);
	
	var _StrokeJs2 = _interopRequireDefault(_StrokeJs);
	
	var _StrokeListJs = __webpack_require__(10);
	
	var _StrokeListJs2 = _interopRequireDefault(_StrokeListJs);
	
	var _DrawingCanvasJs = __webpack_require__(11);
	
	var _DrawingCanvasJs2 = _interopRequireDefault(_DrawingCanvasJs);
	
	var KanjiCanvas = (function () {
		function KanjiCanvas(el) {
			_classCallCheck(this, KanjiCanvas);
	
			this.kc = el;
			this.canvas = new _DrawingCanvasJs2['default'](el.querySelector('canvas'));
			this.clearBtn = this.kc.querySelector('.clear');
			this.submitBtn = this.kc.querySelector('.submit');
			this.strokeList = new _StrokeListJs2['default']();
			this.currStroke = null;
			this.canvas.onDrawStart(this.drawStartHandler.bind(this));
			this.canvas.onDraw(this.drawHandler.bind(this));
			this.canvas.onDrawEnd(this.drawEndHandler.bind(this));
			this.submitCallback = null;
			this.clearBtn.addEventListener('click', this.clear.bind(this));
			this.submitBtn.addEventListener('click', this.submit.bind(this));
		}
	
		KanjiCanvas.prototype.clear = function clear() {
			this.canvas.clear();
			this.strokeList.clear();
		};
	
		KanjiCanvas.prototype.submit = function submit() {
			if (typeof this.submitCallback === 'function') {
				this.submitCallback(this.strokeList);
			}
		};
	
		KanjiCanvas.prototype.drawStartHandler = function drawStartHandler(x, y) {
			this.currStroke = new _StrokeJs2['default'](_pointJs2['default'](x, y));
		};
	
		KanjiCanvas.prototype.drawHandler = function drawHandler(x, y) {
			if (this.currStroke) {
				this.currStroke.add(_pointJs2['default'](x, y));
			}
		};
	
		KanjiCanvas.prototype.drawEndHandler = function drawEndHandler() {
			if (this.currStroke) {
				this.strokeList.add(this.currStroke);
			}
		};
	
		KanjiCanvas.prototype.onSubmit = function onSubmit(callback) {
			this.submitCallback = callback;
		};
	
		return KanjiCanvas;
	})();
	
	exports['default'] = KanjiCanvas;
	module.exports = exports['default'];

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	exports.__esModule = true;
	exports['default'] = p;
	
	function p(x, y) {
		if (arguments.length !== 2) {
			throw new Error('point requires 2 coordinates');
		}
		if (isNaN(x) || isNaN(y)) {
			throw new TypeError('point coordinate is not a number');
		}
		return { 'x': x, 'y': y };
	}
	
	module.exports = exports['default'];

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Stroke = (function () {
		function Stroke() {
			_classCallCheck(this, Stroke);
	
			this.points = arguments.length === 0 ? [] : Array.prototype.slice.call(arguments);
			this.angle = NaN;
		}
	
		Stroke.prototype.add = function add(point) {
			this.points.push(point);
			if (this.points.length > 1) {
				this.angle = this.getAngle();
			}
		};
	
		Stroke.prototype.maxX = function maxX() {
			var i,
			    x = null;
			for (i = 0; i < this.points.length; i++) {
				if (x === null || this.points[i].x > x) {
					x = this.points[i].x;
				}
			}
			return x;
		};
	
		Stroke.prototype.minX = function minX() {
			var i,
			    x = null;
			for (i = 0; i < this.points.length; i++) {
				if (x === null || this.points[i].x < x) {
					x = this.points[i].x;
				}
			}
			return x;
		};
	
		Stroke.prototype.maxY = function maxY() {
			var i,
			    y = null;
			for (i = 0; i < this.points.length; i++) {
				if (y === null || this.points[i].y > y) {
					y = this.points[i].y;
				}
			}
			return y;
		};
	
		Stroke.prototype.minY = function minY() {
			var i,
			    y = null;
			for (i = 0; i < this.points.length; i++) {
				if (y === null || this.points[i].y < y) {
					y = this.points[i].y;
				}
			}
			return y;
		};
	
		Stroke.prototype.area = function area() {
			return (this.maxX() - this.minX()) * (this.maxY() - this.minY());
		};
	
		Stroke.prototype.getAngle = function getAngle() {
			var p1 = this.points[0],
			    p2 = this.points[this.points.length - 1];
			return Math.atan2(p2.y - p1.y, p2.x - p1.x) * 180 / Math.PI;
		};
	
		return Stroke;
	})();
	
	exports["default"] = Stroke;
	module.exports = exports["default"];

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var StrokeList = (function () {
	    function StrokeList() {
	        _classCallCheck(this, StrokeList);
	
	        this.strokes = [];
	    }
	
	    StrokeList.prototype.add = function add(stroke) {
	        this.strokes.push(stroke);
	    };
	
	    StrokeList.prototype.area = function area() {
	        return (this.maxX() - this.minX()) * (this.maxY() - this.minY());
	    };
	
	    StrokeList.prototype.maxX = function maxX() {
	        var i,
	            temp,
	            x = null;
	        for (i = 0; i < this.strokes.length; i++) {
	            temp = this.strokes[i].maxX();
	            if (x === null || temp > x) {
	                x = temp;
	            }
	        }
	        return x;
	    };
	
	    StrokeList.prototype.maxY = function maxY() {
	        var i,
	            temp,
	            y = null;
	        for (i = 0; i < this.strokes.length; i++) {
	            temp = this.strokes[i].maxY();
	            if (y === null || temp > y) {
	                y = temp;
	            }
	        }
	        return y;
	    };
	
	    StrokeList.prototype.minX = function minX() {
	        var i,
	            temp,
	            x = null;
	        for (i = 0; i < this.strokes.length; i++) {
	            temp = this.strokes[i].minX();
	            if (x === null || temp < x) {
	                x = temp;
	            }
	        }
	        return x;
	    };
	
	    StrokeList.prototype.minY = function minY() {
	        var i,
	            temp,
	            y = null;
	        for (i = 0; i < this.strokes.length; i++) {
	            temp = this.strokes[i].minY();
	            if (y === null || temp < y) {
	                y = temp;
	            }
	        }
	        return y;
	    };
	
	    StrokeList.prototype.clear = function clear() {
	        this.strokes = [];
	    };
	
	    return StrokeList;
	})();
	
	exports["default"] = StrokeList;
	module.exports = exports["default"];

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	exports.__esModule = true;
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var DrawingCanvas = (function () {
	    function DrawingCanvas(el) {
	        _classCallCheck(this, DrawingCanvas);
	
	        this.c = el;
	        this.ctx = this.c.getContext('2d');
	        this.moveHandler = null;
	        this.upHandler = null;
	        this.outHandler = null;
	        this.drawStartCallback = null;
	        this.drawCallback = null;
	        this.drawEndCallback = null;
	        this.c.addEventListener('mousedown', this.drawStart.bind(this), false);
	
	        // canvas and line style
	        this.c.height = this.c.clientHeight;
	        this.c.width = this.c.clientWidth;
	        this.ctx.lineCap = 'round';
	        this.ctx.lineJoin = 'round';
	        this.ctx.lineWidth = 3;
	    }
	
	    // given a mouse event, returns mouse coordinates relative to the canvas
	
	    DrawingCanvas.prototype.getOffset = function getOffset(e) {
	        var rect = e.target.getBoundingClientRect();
	        return {
	            x: e.offsetX || e.pageX - rect.left - window.scrollX,
	            y: e.offsetY || e.pageY - rect.top - window.scrollY
	        };
	    };
	
	    // initializes canvas path, registers handlers for other mouse events
	
	    DrawingCanvas.prototype.drawStart = function drawStart(e) {
	        var p = this.getOffset(e);
	        this.c.addEventListener('mousemove', this.moveHandler = this.draw.bind(this), false);
	        this.c.addEventListener('mouseup', this.upHandler = this.drawEnd.bind(this), false);
	        this.c.addEventListener('mouseout', this.outHandler = this.drawEnd.bind(this), false);
	        this.ctx.beginPath();
	        this.ctx.moveTo(p.x, p.y);
	        this.ctx.lineTo(p.x, p.y);
	        this.ctx.stroke();
	        if (typeof this.drawStartCallback === 'function') {
	            this.drawStartCallback(p.x, p.y);
	        }
	    };
	
	    // continues the path to the current mouse coordinates and draws it
	
	    DrawingCanvas.prototype.draw = function draw(e) {
	        var p = this.getOffset(e);
	        this.ctx.lineTo(p.x, p.y);
	        this.ctx.stroke();
	        if (typeof this.drawCallback === 'function') {
	            this.drawCallback(p.x, p.y);
	        }
	    };
	
	    // removes the mouse event handlers
	
	    DrawingCanvas.prototype.drawEnd = function drawEnd(e) {
	        this.c.removeEventListener('mousemove', this.moveHandler);
	        this.c.removeEventListener('mouseup', this.upHandler);
	        this.c.removeEventListener('mouseout', this.outHandler);
	        this.upHandler = null;
	        this.moveHandler = null;
	        this.outHandler = null;
	        if (typeof this.drawEndCallback === 'function') {
	            this.drawEndCallback();
	        }
	    };
	
	    DrawingCanvas.prototype.clear = function clear() {
	        this.ctx.clearRect(0, 0, this.c.width, this.c.height);
	    };
	
	    DrawingCanvas.prototype.onDrawStart = function onDrawStart(callback) {
	        this.drawStartCallback = callback;
	    };
	
	    DrawingCanvas.prototype.onDraw = function onDraw(callback) {
	        this.drawCallback = callback;
	    };
	
	    DrawingCanvas.prototype.onDrawEnd = function onDrawEnd(callback) {
	        this.drawEndCallback = callback;
	    };
	
	    return DrawingCanvas;
	})();
	
	exports['default'] = DrawingCanvas;
	module.exports = exports['default'];

/***/ }
/******/ ]);
//# sourceMappingURL=Spec.bundle.js.map