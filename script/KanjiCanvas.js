import p from './point.js';
import Stroke from './Stroke.js';
import StrokeList from './StrokeList.js';
import DrawingCanvas from './DrawingCanvas.js';

export default class KanjiCanvas {
	constructor(el) {
		this.kc = el;
		this.canvas = new DrawingCanvas(el.querySelector('canvas'));
		this.clearBtn = this.kc.querySelector('.clear');
		this.submitBtn = this.kc.querySelector('.submit');
		this.strokeList = new StrokeList();
		this.currStroke = null;
		this.canvas.onDrawStart(this.drawStartHandler.bind(this));
		this.canvas.onDraw(this.drawHandler.bind(this));
		this.canvas.onDrawEnd(this.drawEndHandler.bind(this));
		this.submitCallback = null;
		this.clearBtn.addEventListener('click', this.clear.bind(this));
		this.submitBtn.addEventListener('click', this.submit.bind(this));
	}

	clear() {
		this.canvas.clear();
		this.strokeList.clear();
	}

	submit() {
		if (typeof this.submitCallback === 'function') {
			this.submitCallback(this.strokeList);
		}
	}

	drawStartHandler(x, y) {
		this.currStroke = new Stroke(p(x, y));
	}

	drawHandler(x, y) {
		if (this.currStroke) {
			this.currStroke.add(p(x, y));
		}
	}

	drawEndHandler() {
		if (this.currStroke) {
			this.strokeList.add(this.currStroke);
		}
	}

	onSubmit(callback) {
		this.submitCallback = callback;
	}	
}