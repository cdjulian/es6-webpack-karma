export default class Stroke {

	constructor()
	{
		this.points = arguments.length === 0 ? [] :
			Array.prototype.slice.call(arguments);
		this.angle = NaN;
	}

	add(point) {
		this.points.push(point);
		if (this.points.length > 1) {
			this.angle = this.getAngle();
		}
	}

	maxX() {
		var i, x = null;
		for (i = 0; i < this.points.length; i++) {
			if (x === null || this.points[i].x > x) {
				x = this.points[i].x;
			}
		}
		return x;
	}

	minX() {
		var i, x = null;
		for (i = 0; i < this.points.length; i++) {
			if (x === null || this.points[i].x < x) {
				x = this.points[i].x;
			}
		}
		return x;
	}

	maxY() {
		var i, y = null;
		for (i = 0; i < this.points.length; i++) {
			if (y === null || this.points[i].y > y) {
				y = this.points[i].y;
			}
		}
		return y;
	}

	minY() {
		var i, y = null;
		for (i = 0; i < this.points.length; i++) {
			if (y === null || this.points[i].y < y) {
				y = this.points[i].y;
			}
		}
		return y;
	}

	area() {
		return (this.maxX() - this.minX()) * (this.maxY() - this.minY());
	}

	getAngle() {
		var p1 = this.points[0],
			p2 = this.points[this.points.length - 1];
		return Math.atan2(p2.y - p1.y, p2.x - p1.x) * 180 / Math.PI;
	}
}